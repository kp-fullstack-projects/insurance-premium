import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class OccupationServiceService {

  constructor(private http:HttpClient) { }
  getOccupations(){
    return this.http.get("http://localhost:3000/occupations");
  }
}
