export enum OccupationRating {
  PROFESSIONAL = 1.0,
  WHITE_COLLAR = 1.25,
  LIGHT_MANUAL = 1.50,
  HEAVY_MANUAL = 1.75
}